# mslog 
## 1.综述
​        一款超轻量级的C日志库，无需依赖额外的库,库的设计目标为"简洁,可移植性强"。

​		库已实际应用到多个实际项目中，测试或移植过的系统有Android，Ubuntu，Centos，Windows，openwrt以及部分嵌入式设备。 

## 2.如何使用
- 参考mslog_api_sample程序;
- 关于编译与安装，可参见make help;

## 3.功能概要
- C语言基本数据类型；
- 日志级别一共有7个级别：
```
	assert（严重错误），error（错误），waring（警告），info(信息)，debug(调试)，verbose(冗余),more(详细);
```
- 每个级别分别实现不同颜色的显示（测试环境为x8664，其他系统期待您的测试适配反馈）,依次为：
```
	red,boldRed,brown,boldBlue,none,green,cyan
```
- 包含基础的数据类型；
- 日志库支持标准终端输出；
- 日志库支持输出日志所在文件行和函数名；
- 日志库支持输出日志信息到日志文件，且保存目录和文件名称可自定义，默认为/var/log/mslog/mslog.txt;
- 日志库支持根据FLAG进行模块过滤；
- 日志库支持2种输出时间：<1>%Y-%M_%D %H:%M:%S；<2>微秒时间；
- 日志库支持多线程安全；
- 日志库支持x8664，aarch64和armv7l架构。

## 4.数据类型

基本数据类型

```
有符号数据类型
	int64   ms_s64  ms_s32  ms_s16 ms_s08 
有符号指针数据类型
	ms_ps64 ms_ps32 ms_ps16 ms_ps08 
无符号数据类型：	
	ms_lu64 ms_u64  ms_u32  ms_u16 ms_u08 
无符号指针数据类型  
	ms_pu64 ms_pu32 ms_pu16 ms_pu08
浮点型
	ms_float ms_double   

空数据类型                    
	ms_void ms_pvoid
```

字节和数组数据类型
```
字节型数据类型
	ms_byte  ms_pbyte
数组性数据类型
	ms_array ms_parray
```

布尔性数据类型
```
布尔性数据类型
	ms_bool ms_pbool
条件为真：
	ms_true
条件为假：
	ms_false
```

字符串性数据类型
```
空指针：
	ms_null
字符串数据类型：
	ms_string  ms_pstring
固定字符串数据类型：
	ms_cstring ms_pcstring
```
分配数组和字符串变量
```
msnew_array(nay,size)
msnew_array08(nay,size)		
msnew_array16(nay,size)		
msnew_array32(nay,size)		
msnew_string(nay)			
```
函数传入参数或数据结构成员类型数据类型
```
ms_in	   参数作为传入值，由函数外部传入供函数内部使用；
ms_out	   参数作为获取值，由函数内部传入供函数外部使用；
ms_io	   参数是复合类型，部分作为传入值，部分作为获取值；
ms_inner   数据结构内部使用，不与外部进行值的交换；
```

数据最大值
```
ms_f64		    64位数据无效值
ms_f32		    32位数据无效值
ms_f16		    16位数据无效值
ms_f08		    08位数据无效值
ms_sf(ms_fnum)  有符号数据最大值，联合 ms_f*型使用；
```
## 5.接口说明

基本函数名

```
mslog_innerapi_*：
	库内部使用，将不详解;
	
ms_string mslog_api_curDTime(ms_string ms_in frm,ms_string ms_out strCurDTime);	
	获取当前的日期和时间字符串，格式通过frm指定；	
	
ms_u64 mslog_api_us(ms_void);	
	获取当前的时间戳，单位微妙	
	
ms_bool mslog_api_timerAsyncSec(time_t * ms_in Etimep,ms_u32 ms_in sec);	
	异步计时，单位秒
	
ms_string mslog_api_sec2Time(ms_u32 ms_in timesec,ms_string outbuf);    
	将秒转化为字符串输出
	
ms_string mslog_api_us2time(ms_s64 ms_in timeus,ms_string outbuf,ms_string frm1,ms_string frm2);	
	将微妙转化为字符串输出
	
ms_s32 mslog_api_strCasestrNum(ms_cstring ms_in strsrc, ms_cstring ms_in strtarget,ms_bool flag_findret,ms_bool flag_case_insensitive);
	查找字符串strtarget在字符串strsrc中出现的次数
		flag_findret:查找到就立马返回，否则会一直查找，并返回出现的次数；
		flag_case_insensitive：忽略字母大小写；
		
ms_void mslog_api_init(ms_u32 ms_in logopt,ms_string ms_in dir,ms_u32 ms_in logfile);
	设置日志选项、日志保存目录和日志文件名称，并初始化日志库环境。该函数支持同一个程序多次调用。
	logopt选项是由32位整形表示：
		前4位表示日志级别，分别为严重错误，错误，警告，信息，调试，冗余，详细，取值分别
		
mslog_level_assert,mslog_level_error,mslog_level_warn,mslog_level_info,mslog_level_debug,mslog_level_verbose,mslog_level_more;
		第5位标识是否开启标准终端输入和输出，开启取值mslog_enable_stdprint;
		第6位标识日志是否添加行和API接口名信息，开启取值mslog_enable_linefunc;
		第7位标识是否将日志打印到日志文件，开启取值mslog_enable_filelog;
		第8位标识是否按为微妙显示日志，开启取值mslog_enable_timeus;
	dir：日志保存目录，最大支持256个字节长度。传入ms_null，则保存到/var/log/mslog目录；
	logfile：日志保存目录文件名称，最大支持256个字节长度。传入ms_null，则保存为mslog.txt；
	
ms_u32 mslog_api_getOpt(ms_void);
	获取当前的logopt选项值；
	
ms_void mslog_api_getOptstring(ms_string ms_in outstr);
	获取当前的logopt选项的字符串描述信息；
	
ms_u32 mslog_api_getLoglevel(ms_void);
	获取当前的日志级别数值；
	
ms_string mslog_api_getLogfile(ms_void);
	获取日志文件的完整路径；
	
ms_void mslog_api_deinit(void);
	清除日志环境；
	
ms_void mslog_api_new(ms_void)
	重新打开日志文件。旧的日志文件将按{filelog}_{时间}.txt进行保存；
	
ms_void mslog_api_info(ms_string ms_out pbuf);
	获取日志库编译功能相关信息；
	
ms_string mslog_api_version(ms_void);
	获取日志库版本号
```

日志打印函数：日志标签由模块定义的宏FLAG统一提供，基本打印
```
ms_fatal(fmt, arg...)
ms_error(fmt, arg...)
ms_waring(fmt, arg...)
ms_info(fmt, arg...)
ms_debug(fmt, arg...)
ms_verbose(fmt, arg...)
ms_more(fmt, arg...) 	
```


日志打印函数：日志标签由模块定义的宏FLAG统一提供，带返回值
```
ms_dbgNoret(ret,frm,arg...)	
ms_warNoret(frm,arg...)		
ms_errNoret(frm,arg...)		
ms_dbgRet(ret,frm,arg...)	
ms_warRet(ret,frm,arg...)	
ms_errRet(ret,frm,arg...)	
```
日志打回函数：日志标签由模块定义的宏FLAG统一提供，带跳转功能
```c
ms_dbgGoto(gval,frm,arg...)		
ms_infoGoto(gval,frm,arg...)	
ms_warGoto(gval,frm,arg...)	
ms_errGoto(gval,frm,arg...)		
ms_errGotoRet(gval,ret,frm,arg...)  
```


日志打回函数：日志标签由模块定义的宏FLAG统一提供，带程序退出功能
```
ms_errExit(ret,frm,arg...)		
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，调试用。包括函数进入、退出和行打印
```
ms_funcEnter
ms_funcLeave
ms_funcLine	
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，功能不支持打印
```
ms_funcNosupport(arg)		
	功能不支持日志，功能名由arg传入
ms_funcNosupports	
	功能不支持日志，功能名是打印所在的函数
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，功能未授权打印
```
ms_funcUnauthent(arg)	
	功能未授权日志，功能名由arg传入
ms_funcUnauthents
	功能未授权日志，功能名是打印所在的函数
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，功能需要修复打印
```
ms_fix(frm,arg...)		
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，功能函数已被抛弃建议用新功能函数
```
ms_deprecated(oldfunc_name,newfunc_name)
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，数字到字符串的打印
```
64位num转换为字符串返回
	ms_num2str(num) 			

位或字节到字符串的转换
	ms_bitbyte64(num_h,num_l) 					
	ms_bitbyte32(num_h,num_l) 				 
	ms_bitbyte64_unit(num_h,num_l,unit)
	ms_bitbyte32_unit(num_h,num_l,unit) 
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，BUF打印
```
ms_bufHex(description, buf, len)	
	将BUF中的数据以16进制方式显示，每行显示16个字节，不足16字节补0，日志级别为调试
ms_bufHexAscii(description, buf, len)
	将BUF中的数据以16进制+ASCII方式显示，每行显示16个字节，不足16字节补0，日志级别为调试
ms_bufHexErr(description, buf, len)				
	将BUF中的数据以16进制方式显示，每行显示16个字节，不足16字节补0，日志级别为错误
ms_bufAscii(description, buf, len)	
	将BUF中的数据以ASCII方式显示，每行显示16个字节，不足16字节补0，日志级别为调试
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，带开关功能
```
ms_enErr(enable,fmt, arg...)
ms_enDbg(enable,fmt, arg...)
ms_enInfo(enable,fmt, arg...)
ms_enVerbose(enable,fmt, arg...)
ms_enHlight(enable,fmt, arg...)	
ms_enBufHex(enable,description, buf,len)	
ms_enBufAscii(enable,description, buf,len) 
```

日志打回函数：日志标签由模块定义的宏FLAG统一提供，参数检测
```
ms_bufcheck(dbuf)					
ms_bufcheckDes(buf,fmt,arg...) 		
ms_bufcheckGoto(gval,dbuf)			
ms_bufcheckGotoDes(gval,buf,fmt,arg...) 
ms_bufcheckNoret(dbuf)				
ms_bufcheckRet(ret,dbuf)			
ms_bufcheckRetDes(ret,buf,fmt,arg...)  

ms_pamcheck(buf,strname) 			
ms_pamcheckRet(ret,buf,strname)		
ms_pamcheckGoto(gval,buf,strname)	
ms_pamNocheck(dbuf)
```

日志功能选项
```
mslog_enable_stdprint
mslog_enable_linefunc
mslog_enable_filelog
mslog_enable_timeus
```

文件日志功能

```
ms_logfile(filename,fmt,arg...)
	向filename文件中，以追加写入内容
ms_logfileMsg(filename,msg,fmt,arg...)
	向filename文件中，以追加写入内容。并且将写入的内容赋予内存块msg，用于后续的处理
```

## 6.关于

 作者:smartavs

 邮箱: msavskit@163.com
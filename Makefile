#####custom
prefix_x8664=/usr/local/mscore
prefix_aarch64=/usr/local/mscore_aarch64
prefix_armv7l=/usr/local/mscore_armv7l
export PKG_CONFIG_PATH:=${prefix_x8664}/lib/pkgconfig:${prefix_aarch64}/lib/pkgconfig:${prefix_armv7l}/lib/pkgconfig:$PKG_CONFIG_PATH

include platform/*.mk
#########
PROGRAM_NAME=libmslog

CFLAGS_SHARE=-shared  -fPIC
DBG_FLAG=-g -rdynamic

OUT_LIB=out/lib
OUT_BIN=out/bin
OUT_INCLUDE=out/include/$(PROGRAM_NAME)

OUT_JNISRC=out/$(PROGRAM_NAME)jni_source
OUT_JNILIB=$(OUT_JNISRC)/lib
OUT_JNIINCLUDE=$(OUT_JNISRC)/include

D_BASEAPI_FLAGS= \
    -DOS_LINUX_SOC \
    -D__STDC_FORMAT_MACROS

C_INCLUDE=${D_BASEAPI_FLAGS} -Isrc
C_SRC=src/mslog.c

default: x8664
clean:
	rm  out -fr
help:
	@echo "USAGE:make target"
	@echo "    all			Produce the ${PROGRAM_NAME}.so,and install it"
	@echo "    clean		Clear out"
	@echo "    mslog_api_sample	Sample program with ${PROGRAM_NAME}"

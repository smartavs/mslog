libmslog_aarch64_CC=aarch64-linux-gnu-gcc
libmslog_aarch64_SRC=${C_SRC}
libmslog_aarch64_CFLAGS=${C_INCLUDE} ${CFLAGS_SHARE} 

	
aarch64:libmslog_aarch64 install_aarch64

libmslog_aarch64: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS) -o $(OUT_LIB)/$(PROGRAM_NAME).so 

libmslog_aarch64_gdb: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  ${DBG_FLAG} -o $(OUT_LIB)/libmslog_aarch64.so 
	
install_aarch64: 
	mkdir -p  ${prefix_aarch64}/lib/ 
	mkdir -p  ${prefix_aarch64}/include/   
	mkdir -p  ${prefix_aarch64}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so      ${prefix_aarch64}/lib/     
	cp -fr $(OUT_INCLUDE) 	                  ${prefix_aarch64}/include/	     
	cp -fr platform/$(PROGRAM_NAME)_aarch64.pc         ${prefix_aarch64}/lib/pkgconfig/
	
uninstall_aarch64:
	rm -fr ${prefix_aarch64}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_aarch64}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_aarch64}/lib/pkgconfig/$(PROGRAM_NAME)_aarch64.pc	

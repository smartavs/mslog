mslog_api_sample_CC=${CC}
mslog_api_sample_SRC=${C_SRC} mslog_api_sample.c
	
mslog_api_sample_CFLAGS=${C_INCLUDE}

libmslog_CC=${CC}
libmslog_SRC=${C_SRC}
libmslog_CFLAGS=${C_INCLUDE} ${CFLAGS_SHARE} 

x8664:      libmslog     install
x8664_gdb:  libmslog_gdb install

libmslog: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS)  -o $(OUT_LIB)/$(PROGRAM_NAME).so 
libmslog_gdb: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$(libmslog_CC)  $(libmslog_SRC)  $(libmslog_CFLAGS) ${DBG_FLAG} -o $(OUT_LIB)/$(PROGRAM_NAME).so  

sample: mslog_api_sample
mslog_api_sample: 
	mkdir -p $(OUT_BIN)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS) -lpthread ${DBG_FLAG} -o $(OUT_BIN)/$@ 

install:
	mkdir -p  ${prefix_x8664}/lib/ 
	mkdir -p  ${prefix_x8664}/include/   
	mkdir -p  ${prefix_x8664}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so 	${prefix_x8664}/lib/     
	cp -fr $(OUT_INCLUDE) 			${prefix_x8664}/include/	     
	cp -fr platform/$(PROGRAM_NAME).pc	${prefix_x8664}/lib/pkgconfig/
uninstall:
	rm -fr ${prefix_x8664}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_x8664}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_x8664}/lib/pkgconfig/$(PROGRAM_NAME).pc

libmslog_armv7l_PATH=/home/sugao/H2_toolchain/toolchain/4.9.3
libmslog_armv7l_CC=${libmslog_armv7l_PATH}/bin/arm-cortexa9-linux-gnueabihf-gcc
libmslog_armv7l_SRC=${C_SRC}
libmslog_armv7l_CFLAGS=${C_INCLUDE} ${CFLAGS_SHARE} 

armv7l:libmslog_armv7l install_armv7l
libmslog_armv7l: 
	mkdir -p $(OUT_LIB)
	mkdir -p $(OUT_INCLUDE)
	cp -fr src/*.h $(OUT_INCLUDE)
	$($@_CC)  $($@_SRC)  $($@_CFLAGS) ${DBG_FLAG} -o $(OUT_LIB)/$(PROGRAM_NAME).so 
install_armv7l: 
	mkdir -p  ${prefix_armv7l}/lib/ 
	mkdir -p  ${prefix_armv7l}/include/   
	mkdir -p  ${prefix_armv7l}/lib/pkgconfig/ 
	cp -fr $(OUT_LIB)/$(PROGRAM_NAME).so      ${prefix_armv7l}/lib/     
	cp -fr $(OUT_INCLUDE) 	                  ${prefix_armv7l}/include/	     
	cp -fr platform/$(PROGRAM_NAME)_armv7l.pc         ${prefix_armv7l}/lib/pkgconfig/
uninstall_armv7l:
	rm -fr ${prefix_armv7l}/lib/$(PROGRAM_NAME).so 
	rm -fr ${prefix_armv7l}/include/$(PROGRAM_NAME)
	rm -fr ${prefix_armv7l}/lib/pkgconfig/$(PROGRAM_NAME)_armv7l.pc		

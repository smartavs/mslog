#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/prctl.h>
#include "mslog.h"

#define FLAG "LOG_SAMPLE"
void * mslog_cbapi_tdTest(void *arg)
{
	ms_byte *tdName=arg;
	if(0!=prctl(PR_SET_NAME, tdName)){
		ms_waring("prctl(PR_SET_NAME):%s  failed", tdName);
	}
	ms_u32 time=0;
	while(1){
		ms_info("[%s]This is mThread logfile test.time=%u",tdName,time++);
		usleep(20);
	}
}
ms_void main(ms_void)
{
	//设置日志级别为more；打开标准输出、打印所在行数和函数名、文件日志功能；
    //设置日志目录为：/tmp/mslog;
    //设置日志文件为：mslog_sample.txt;
    //FLAG为TAG_TEST3或TAG_TEST2的日志，进行打印;
	mslog_api_init((mslog_level_more|mslog_enable_stdprint|mslog_enable_linefunc|mslog_enable_filelog),
		"/tmp/mslog","mslog_sample.txt",ms_null);
	
	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：获取相关信息，包括版本号，当前日志文件的完整路径，当前生效的OPTION%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	ms_array opt_string[512]={0};
	ms_array infofunc_string[512]={0};
	mslog_api_getOptstring(opt_string);
	mslog_api_info(infofunc_string);
	ms_debug ("version:%s,%s",mslog_api_version(),infofunc_string);
	ms_debug ("logfile:%s",mslog_api_getLogfile());
	ms_debug ("opt:%d=%s",mslog_api_getOpt(),opt_string);
	
	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：FLAG过滤功能，只显示TAG_TEST3|TAG_TEST2%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	mslog_api_init((mslog_level_more|mslog_enable_stdprint|mslog_enable_linefunc|mslog_enable_filelog),
		"/tmp/mslog","mslog_sample.txt","TAG_TEST3|TAG_TEST2");
	ms_ldebug("TAG_TEST1" , "--------TAG_TEST1") ;
	ms_ldebug("TAG_TEST2" , "--------TAG_TEST2") ;
	ms_ldebug("TAG_TEST3" , "--------TAG_TEST3") ;
	ms_ldebug("TAG_TEST4" , "--------TAG_TEST4") ;
	//mslog_api_new(ms_void);
	mslog_api_init((mslog_level_more|mslog_enable_stdprint|mslog_enable_linefunc|mslog_enable_filelog),
		"/tmp/mslog","mslog_sample.txt",ms_null);
	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：不同打印级别的颜色%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	ms_fatal ("fatal log show");
	ms_error ("error log show");
	ms_waring ("waring log show");
	ms_info ("info log show");
	ms_debug ("debug log show");
	ms_verbose ("verbose log show");
	ms_more("more log show");

	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：4种不同打印的区别%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	ms_verbose2("%s--------------------[ms_more]文件日志、级别颜色、日志时间、日志级别、日志FLAG、支持打印FLAG过滤、打印所在行和函数名打印、自动加换行符%s",LINE_END,LINE_END);
	ms_more ("[log show1]");ms_more ("[log show2]");
	ms_verbose2("%s--------------------[ms_more1]文件日志、级别颜色、日志时间、日志级别、日志FLAG、支持打印FLAG过滤%s",LINE_END,LINE_END);
	ms_more1 ("[log show1]");ms_more1 ("[log show2]");
	ms_verbose2("%s--------------------[ms_more2]文件日志、级别颜色%s",LINE_END,LINE_END);
	ms_more2 ("[log show1]");ms_more2 ("[log show2]");
	ms_verbose2("%s--------------------[ms_more3]级别颜色、自动加换行符%s",LINE_END,LINE_END);
	ms_more3 ("[log show1]");ms_more3 ("[log show2]");
	
	ms_info2("===============================================================%s",LINE_END);	
	ms_info2("测试：输出字符串到指定文件%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	ms_logfile("/tmp/mslog/logfile.txt", "This is %s  function test","ms_logfile");
	
	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：输出字符串到指定文件，并获取字符串内容%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	ms_byte msg[256]={0};
	ms_logfileMsg("/tmp/mslog/logfilemsg.txt", msg,"This is %s  function test","ms_logfile");
	ms_debug("%s",msg);

	mslog_api_color();
	sleep(10);
//wait 	
	ms_info2("===============================================================%s",LINE_END);
	ms_info2("测试：多线程快速打印测试%s",LINE_END);
	ms_info2("===============================================================%s",LINE_END);
	#define MAXNUM_TD	16
	pthread_t thread_id[MAXNUM_TD]={0};
	ms_byte tdName[MAXNUM_TD][256]={0};
	for(ms_s08 index=0;index<MAXNUM_TD;index++){
		sprintf(tdName[index], "ID%d", index);
		if(0!=pthread_create((pthread_t *)&thread_id[index], ms_null, mslog_cbapi_tdTest,tdName[index])){
			ms_errNoret( "TD%d:create thread failed",index);
		}
	}
	ms_u32 time=0;
	while(1){
		ms_info("[MAIN]This is loop logfile test:time=%u",(time++));
		usleep(100);
	}

	//释放日志资源；
	mslog_api_deinit();
}

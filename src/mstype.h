#ifndef MSTYPE_H
#define MSTYPE_H

#include <inttypes.h>


/*********************************************************
基本数据类型
**********************************************************/
//有符号数据类型
typedef signed long long 	int64;
typedef int64_t				ms_s64;
typedef signed int			ms_s32;
typedef signed short		ms_s16;
typedef signed char			ms_s08;
//有符号指针数据类型
typedef ms_s64 *			ms_ps64;
typedef ms_s32 *			ms_ps32;
typedef ms_s16 *			ms_ps16;
typedef ms_s08 *			ms_ps08;

//无符号数据类型
typedef long unsigned int 	ms_lu64;
typedef uint64_t 			ms_u64;
typedef unsigned int		ms_u32;
typedef unsigned short		ms_u16;
typedef unsigned char		ms_u08;
//无符号指针数据类型
typedef ms_u64 *			ms_pu64;
typedef ms_u32 *			ms_pu32;
typedef ms_u16 *			ms_pu16;
typedef ms_u08 *			ms_pu08;
//浮点型
typedef float				ms_float;
typedef double				ms_double;
//空数据类型 
typedef void 				ms_void;
typedef void 	* 			ms_pvoid;


/*********************************************************
字节和数组数据类型
**********************************************************/
typedef	char			ms_byte;
typedef	ms_byte *		ms_pbyte;
typedef	ms_byte 		ms_array;
typedef	ms_byte *		ms_parray;


/*********************************************************
布尔性数据类型
**********************************************************/
typedef ms_s08			ms_bool;
#define ms_false		0
#define ms_true			1

#define ms_restart		2



/*********************************************************
字符串性数据类型
**********************************************************/
#define ms_null			NULL
typedef char *			ms_string;
typedef ms_string *		ms_pstring;
typedef const char *	ms_cstring;
typedef ms_cstring *	ms_pcstring;	

/*********************************************************
分配数组和字符串变量
**********************************************************/
#define msnew_array(nay,size)		ms_array nay[size]={0};
#define msnew_array08(nay,size)		ms_array nay[size]={0};
#define msnew_array16(nay,size)		ms_s16 nay[size]={0};
#define msnew_array32(nay,size)		ms_s32 nay[size]={0};
#define msnew_string(nay)			ms_string nay=ms_null;

/*********************************************************
函数传入参数或数据结构成员类型数据类型
**********************************************************/
#define ms_in	
#define ms_out	
#define ms_io	
#define ms_inner	

/*********************************************************
数据最大值
**********************************************************/
#define ms_f64		0xFFFFFFFFFFFFFFFF
#define ms_f32		0xFFFFFFFF
#define ms_f16		0xFFFF
#define ms_f08		0xFF
//有符号数据最大值，联合 ms_f*型使用；
#define ms_sf(ms_fnum)	((ms_fnum>>1)-1)
#endif


#ifndef MSLOG_H
#define MSLOG_H
#ifdef __cplusplus
   extern "C"{
#endif
#include "mslog_inner.h"

/*****************************************************************************
 Description : 
*****************************************************************************/
INTERFACE_LOG  ms_string 
	mslog_api_curDTime(ms_string ms_in frm,ms_string ms_out strCurDTime);
INTERFACE_LOG ms_u64 
	mslog_api_upUs(ms_void);
INTERFACE_LOG ms_bool 
	mslog_api_timerAsyncSec(time_t * ms_in Etimep,ms_u32 ms_in sec);
INTERFACE_LOG ms_string 
	mslog_api_sec2Time(ms_u32 ms_in timesec,ms_string outbuf);
INTERFACE_LOG ms_string 
	mslog_api_us2time(ms_s64 ms_in timeus,ms_string outbuf,ms_string frm1,ms_string frm_sec,ms_string frm_ms,ms_string frm_us);
INTERFACE_LOG ms_s32 
	mslog_api_strCasestrNum(ms_cstring ms_in strsrc, ms_cstring ms_in strtarget,ms_bool  ms_in flag_findret,ms_bool  ms_in flag_case_insensitive);
INTERFACE_LOG ms_void 
	mslog_api_init(ms_u32 ms_in logopt,ms_string ms_in dir,ms_string ms_in logfile,ms_string ms_in filter_tags);
INTERFACE_LOG ms_u32 
	mslog_api_getOpt(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_getOptstring(ms_string ms_in outstr);
INTERFACE_LOG ms_u32 
	mslog_api_getLoglevel(ms_void);
INTERFACE_LOG ms_string 
	mslog_api_getLogfile(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_deinit(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_new(ms_void);
INTERFACE_LOG ms_void 
	mslog_api_info(ms_string ms_out pbuf);
INTERFACE_LOG ms_string 
	mslog_api_version(ms_void);

/*****************************************************************************
打印接口
*****************************************************************************/
//功能：文件日志、级别颜色、日志时间、日志级别、日志FLAG、支持打印FLAG过滤、打印所在行和函数名打印、自动加换行符
#define ms_fatal(fmt, arg...) 		ms_lfatal(FLAG, fmt, ##arg)
#define ms_error(fmt, arg...) 		ms_lerror(FLAG, fmt, ##arg)
#define ms_waring(fmt, arg...) 		ms_lwaring(FLAG, fmt, ##arg)
#define ms_info(fmt, arg...) 		ms_linfo(FLAG, fmt, ##arg)
#define ms_debug(fmt, arg...) 		ms_ldebug(FLAG, fmt, ##arg)
#define ms_verbose(fmt, arg...) 	ms_lverbose(FLAG, fmt, ##arg)
#define ms_more(fmt, arg...) 		ms_lmore(FLAG, fmt, ##arg)
//功能：文件日志、级别颜色、日志时间、日志级别、日志FLAG、支持打印FLAG过滤
#define ms_fatal1(fmt, arg...) 		ms_lfatal1(FLAG, fmt, ##arg)
#define ms_error1(fmt, arg...) 		ms_lerror1(FLAG, fmt, ##arg)
#define ms_waring1(fmt, arg...) 	ms_lwaring1(FLAG, fmt, ##arg)
#define ms_info1(fmt, arg...) 		ms_linfo1(FLAG, fmt, ##arg)
#define ms_debug1(fmt, arg...) 		ms_ldebug1(FLAG , fmt, ##arg)
#define ms_verbose1(fmt, arg...) 	ms_lverbose1(FLAG, fmt, ##arg)
#define ms_more1(fmt, arg...) 		ms_lmore1(FLAG, fmt, ##arg)
//功能：文件日志、级别颜色
#define ms_fatal2(fmt, arg...) 		LOG_DBG2( mslog_level_assert, fmt, ##arg)
#define ms_error2( fmt, arg...) 	LOG_DBG2( mslog_level_error, fmt, ##arg)
#define ms_waring2(fmt, arg...) 	LOG_DBG2( mslog_level_warn, fmt, ##arg)
#define ms_info2(fmt, arg...) 		LOG_DBG2( mslog_level_info, fmt, ##arg)
#define ms_debug2( fmt, arg...) 	LOG_DBG2( mslog_level_debug, fmt, ##arg)
#define ms_verbose2(fmt, arg...) 	LOG_DBG2( mslog_level_verbose, fmt, ##arg)
#define ms_more2(fmt, arg...) 		LOG_DBG2( mslog_level_more, fmt, ##arg)
//功能：级别颜色、自动加换行符
#define ms_fatal3(fmt, arg...) 		LOG_DBG3( mslog_level_assert, fmt, ##arg)
#define ms_error3( fmt, arg...) 	LOG_DBG3( mslog_level_error, fmt, ##arg)
#define ms_waring3(fmt, arg...) 	LOG_DBG3( mslog_level_warn, fmt, ##arg)
#define ms_info3(fmt, arg...) 		LOG_DBG3( mslog_level_info, fmt, ##arg)
#define ms_debug3( fmt, arg...) 	LOG_DBG3( mslog_level_debug, fmt, ##arg)
#define ms_verbose3(fmt, arg...) 	LOG_DBG3( mslog_level_verbose, fmt, ##arg)
#define ms_more3(fmt, arg...) 		LOG_DBG3( mslog_level_more, fmt, ##arg)

/*****************************************************************************
 返回打印
*****************************************************************************/
#define ms_dbgNoret(ret,frm,arg...)	ms_debug(frm,##arg); return;
#define ms_warNoret(frm,arg...)		ms_waring(frm,##arg);return;
#define ms_errNoret(frm,arg...)		ms_error(frm,##arg); return;
#define ms_dbgRet(ret,frm,arg...)	ms_debug(frm,##arg); return ret;
#define ms_warRet(ret,frm,arg...)	ms_waring(frm,##arg);return ret;
#define ms_errRet(ret,frm,arg...)	ms_error(frm,##arg); return ret;

/*****************************************************************************
goto跳转打印
*****************************************************************************/
#define ms_verGoto(gval,frm,arg...)			ms_verbose(frm,##arg); goto gval;
#define ms_dbgGoto(gval,frm,arg...)			ms_debug(frm,##arg); goto gval;
#define ms_infoGoto(gval,frm,arg...)		ms_info(frm,##arg); goto gval;
#define ms_warGoto(gval,frm,arg...)			ms_waring(frm,##arg); goto gval;
#define ms_errGoto(gval,frm,arg...)			ms_error(frm,##arg); goto gval;
#define ms_errGotoRet(gval,ret,frm,arg...)	ms_error(frm,##arg); ret=-1;goto gval;

/*****************************************************************************
程序退出打印
*****************************************************************************/
#define ms_errExit(ret,frm,arg...)	ms_error( frm,##arg); exit(ret);

/*****************************************************************************
 调试用。包括函数进入、退出和行打印
*****************************************************************************/
#define ms_funcEnter		ms_debug(">>>>>>>>>>Enter %s ",__FUNCTION__);
#define ms_funcLeave		ms_debug("<<<<<<<<<<Leave %s ",__FUNCTION__);
#define ms_funcLine			ms_debug("[%s]=========line:%d ",__FUNCTION__,__LINE__);

/*****************************************************************************
 功能不支持
*****************************************************************************/
#define ms_funcNosupport(arg)		ms_waring("No support %s",arg);
#define ms_funcNosupports		 	ms_waring("No support %s",__FUNCTION__);

/*****************************************************************************
 功能未授权打印
*****************************************************************************/
#define ms_funcUnauthent(arg)		ms_waring("No authent %s",arg);
#define ms_funcUnauthents		 	ms_waring("No authent %s",__FUNCTION__);

/*****************************************************************************
 功能需要修复打印
*****************************************************************************/
#define ms_fix(frm,arg...)			ms_waring("[%s]" frm ",please fix me","smartavs",##arg);

/*****************************************************************************
 功能函数已被抛弃建议用新功能函数
*****************************************************************************/
#define ms_deprecated(oldfunc_name,newfunc_name) 	ms_waring("%s is deprecated, uses %s to instead" ,oldfunc_name,newfunc_name);

/*****************************************************************************
数字到字符串的打印
*****************************************************************************/
//64位数字到字符串的转换
#define ms_num2str(num) 						mslog_innerapi_num2str((char[32]){0}, num)
//位或字节到字符串的转换
#define ms_bitbyte64(num_h,num_l) 				mslog_innerapi_bitByte64((char[32]){0},num_h, num_l,ms_null)			
#define ms_bitbyte32(num_h,num_l) 				mslog_innerapi_bitByte32((char[32]){0},num_h, num_l,ms_null)	
#define ms_bitbyte64_unit(num_h,num_l,unit) 	mslog_innerapi_bitByte64((char[32]){0},num_h, num_l,unit)			
#define ms_bitbyte32_unit(num_h,num_l,unit) 	mslog_innerapi_bitByte32((char[32]){0},num_h, num_l,unit)

/*****************************************************************************
BUF内容打印
*****************************************************************************/
#define ms_bufHex(description, buf, len)		mslog_innerapi_bufHex(description, buf, len,__FUNCTION__,__LINE__)
#define ms_bufHexAscii(description, buf, len)	mslog_innerapi_bufHexAscii(description, buf, len,__FUNCTION__,__LINE__)
#define ms_bufHexErr(description, buf, len)		mslog_innerapi_bufHexErr(description, buf, len,__FUNCTION__,__LINE__)
#define ms_bufAscii(description, buf, len)		mslog_innerapi_bufAscii(description, buf, len,__FUNCTION__,__LINE__)

/*****************************************************************************
 带开关功能的打印
*****************************************************************************/
#define ms_enErr(enable,fmt, arg...)				if(enable)ms_error( fmt, ##arg);
#define ms_enWar(enable,fmt, arg...)				if(enable)ms_waring( fmt, ##arg);
#define ms_enDbg(enable,fmt, arg...)				if(enable)ms_debug( fmt, ##arg);
#define ms_enInfo(enable,fmt, arg...)				if(enable)ms_info( fmt, ##arg);
#define ms_enVerbose(enable,fmt, arg...)			if(enable)ms_verbose( fmt, ##arg);
#define ms_enHlight(enable,fmt, arg...)				if(enable)ms_info( fmt, ##arg);
#define ms_enBufHex(enable,description, buf,len)	if(enable)ms_bufHex(description, buf,len);
#define ms_enBufAscii(enable,description, buf,len)	if(enable)ms_bufAscii(description, buf,len);

/*****************************************************************************
检测打印
*****************************************************************************/
#define ms_bufcheck(dbuf)						bufcheck(dbuf)
#define ms_bufcheckDes(buf,fmt,arg...) 			bufcheck_des(buf,fmt,##arg) 
#define ms_bufcheckGoto(gval,dbuf)				bufcheck_goto(gval,dbuf)
#define ms_bufcheckGotoDes(gval,buf,fmt,arg...)	bufcheck_goto_des(gval,buf,fmt,##arg) 
#define ms_bufcheckNoret(dbuf)					bufcheck_noret(dbuf)
#define ms_bufcheckRet(ret,dbuf)				bufcheck_ret(ret,dbuf)
#define ms_bufcheckRetDes(ret,buf,fmt,arg...)	bufcheckret_des(ret,buf,fmt,##arg)

#define ms_pamcheck(buf,strname) 			bufcheck_des(buf,"Param error####%s",strname)
#define ms_pamcheckRet(ret,buf,strname)		bufcheckret_des(ret,buf,"Param error####%s",strname)
#define ms_pamcheckGoto(gval,buf,strname)	bufcheck_goto_des(gval,buf,"Param error####%s",strname)
#define ms_pamNocheck(dbuf)

/*****************************************************************************
 日志功能选项
*****************************************************************************/
//ENLOGLevel
#define mslog_enable_stdprint	(1<<4)
#define mslog_enable_linefunc	(1<<5)
#define mslog_enable_filelog	(1<<6)
#define mslog_enable_timeus		(1<<7)

/*****************************************************************************
 文件日志功能
*****************************************************************************/
#define ms_logfile(filename,fmt, arg...) 			mslog_api_toFile( filename,NULL,fmt, ##arg)
#define ms_logfileMsg(filename,msg,fmt, arg...) 	mslog_api_toFile( filename,msg,fmt, ##arg)
#define ms_enLogfile(enable,filename,fmt, arg...) 	if(enable){mslog_api_toFile( filename,NULL,fmt, ##arg);}

#ifdef __cplusplus
}
#endif

#endif

